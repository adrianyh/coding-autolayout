//
//  ViewController.swift
//  Coding Autolayout
//
//  Created by Adrian Young-Hoon on 2017-09-11.
//  Copyright © 2017 Rapid MAP Media. All rights reserved.
//

import UIKit

class ViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout  {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        navigationItem.title = "HOME"
        
        collectionView?.backgroundColor = UIColor.white
        collectionView?.register(CustomCell.self, forCellWithReuseIdentifier: "CellID")
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellID", for: indexPath)
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

class CustomCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    let customImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.brown
        return imageView
    }()
    
    let customImageViewSmall: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.purple
        return imageView
    }()
    
    let customLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.red
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let customTextView: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = UIColor.orange
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        return view
    }()
    
    func setupViews() {
        addSubview(customImageView)
        addSubview(customImageViewSmall)
        addSubview(customLabel)
        addSubview(customTextView)
        addSubview(separatorView)
        
        addConstraintsWithVisualFormat(format: "H:|-16-[v0]-16-|", views: customImageView)
        addConstraintsWithVisualFormat(format: "H:|-16-[v0(44)]", views: customImageViewSmall)
        
        addConstraintsWithVisualFormat(format: "V:|-16-[v0]-8-[v1(44)]-16-[v2(1)]|", views: customImageView, customImageViewSmall, separatorView)
        
        addConstraintsWithVisualFormat(format: "H:|[v0]|", views: separatorView)
        
        // Created UIView extension below
        /*
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[v0]-16-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": customImageView]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-16-[v0]-16-[v1(1)]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": customImageView, "v1": separatorView]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v0]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": separatorView]))
        */
        
        // customLabel
        // Top Constraint
        addConstraint(NSLayoutConstraint(item: customLabel, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: customImageView, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 8))
        // Left Constraint
        addConstraint(NSLayoutConstraint(item: customLabel, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: customImageViewSmall, attribute: NSLayoutAttribute.right, multiplier: 1, constant: 8))
        // Right Constraint
        addConstraint(NSLayoutConstraint(item: customLabel, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: customImageView, attribute: NSLayoutAttribute.right, multiplier: 1, constant: 0))
        // Height Constraint
        addConstraint(NSLayoutConstraint(item: customLabel, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.height, multiplier: 0, constant: 20))
        
        // customTextView
        // Top Constraint
        addConstraint(NSLayoutConstraint(item: customTextView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: customLabel, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 8))
        // Left Constraint
        addConstraint(NSLayoutConstraint(item: customTextView, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: customImageViewSmall, attribute: NSLayoutAttribute.right, multiplier: 1, constant: 8))
        // Right Constraint
        addConstraint(NSLayoutConstraint(item: customTextView, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: customImageView, attribute: NSLayoutAttribute.right, multiplier: 1, constant: 0))
        // Height Constraint
        addConstraint(NSLayoutConstraint(item: customTextView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.height, multiplier: 0, constant: 20))
        
//        customImageView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension UIView {
    func addConstraintsWithVisualFormat(format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
        
    }
}



